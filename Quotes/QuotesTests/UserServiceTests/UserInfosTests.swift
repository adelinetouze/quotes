//
//  UserInfosTests.swift
//  QuotesTests
//
//  Created by Adeline Touzé on 06/05/2018.
//  Copyright © 2018 AT. All rights reserved.
//

import XCTest
import RealmSwift
import ObjectMapper
/** test
 setup :
 - current User in DB with partial information
 
 teardown :
 - remove current User in DB
 
 usecase : json with API error object
 > errorHandler with APIError object
 > successHandler not called
 
 usecase : json with User object
 - for user already in DB
 > errorHandler not called
 > succesHandler with login
 > Old User in DB with new datas
 - for user not in DB
 > errorHandler not called
 > succesHandler with login
 > New User in DB with datas
 
 usecase : json with random data
 > errorHandler with nil object
 > succesHandler not called
 */

@testable import Quotes

class UserInfosTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let json = [
            "User-Token" : "i4dwXAUNfSUEaTP90K5RWOWLap5cvc9mtjfpvA+c0uufP5GYnpYZUO4k5pTEpwsJHVHNhY5lbLn0cDRpiwsOKA==",
            "login" : "user_login",
            "email" : "user_email"
        ]
        
        if let user = Mapper<User>().map(JSON: json) {

        let realm = try! Realm()
        try! realm.write {
            realm.add(user, update : true)
        }
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
        
        super.tearDown()
    }
    
    func testAPIError() {
        
        let json = [
            "error_code" : 21,
            "message": "Invalid login or password."
            ] as [String : Any]
        
        let ws = UserService()
        ws.successHandler = { response in
            XCTFail()
        }
        
        ws.errorHandler = { error in
            XCTAssertNotNil(error)
            XCTAssert(error!.isKind(of: APIError.self))
        }
        
        ws.parseUserInfosResponse(login: "user_login", json: json)
    }
    
    
    func testgetCurrentUserInfos() {
        let login = "user_login"
        let json = [
            "login" : "user_login",
            "pic_url" : "https://pbs.twimg.com/profile_images/2160924471/Screen_Shot_2012-04-23_at_9.23.44_PM_.png",
            "public_favorites_count" : 520,
            "followers" : 12,
            "following" : 23,
            "pro" : true,
            "account_details": [
                "email" : "user_email",
                "private_favorites_count" : 22,
                "active_theme_id" : 1,
                "pro_expiration" : "2015-03-13T07:19:06.133-05:00"
            ]
            ] as [String : Any]
        
        let ws = UserService()
        
        ws.errorHandler = { _ in
            XCTFail()
            
        }
        
        ws.successHandler = { response in
            
            guard let responseLogin : String = response as? String else {
                XCTFail()
                return
            }
            
            XCTAssertEqual(login, responseLogin)
            let realm = try! Realm()
            let user = realm.object(ofType: User.self, forPrimaryKey: login)
            XCTAssertNotNil(user)
            //XCTAssertNotNil(user?.accountDetails) // TODO Manage account details
            
        }
        
        ws.parseUserInfosResponse(login: login, json: json)
    }
    
    func testRandomUserInfos() {
        let login = "gose"
        let json = [
            "login" : "gose",
            "pic_url" : "https://pbs.twimg.com/profile_images/2160924471/Screen_Shot_2012-04-23_at_9.23.44_PM_.png",
            "public_favorites_count" : 520,
            "followers" : 12,
            "following" : 23,
            "pro" : true
            ] as [String : Any]
        
        let ws = UserService()
        
        ws.errorHandler = { _ in
            XCTFail()
            
        }
        
        ws.successHandler = { response in
            
            guard let responseLogin : String = response as? String else {
                XCTFail()
                return
            }
            
            XCTAssertEqual(login, responseLogin)
            let realm = try! Realm()
            let user = realm.object(ofType: User.self, forPrimaryKey: login)
            XCTAssertNotNil(user)
            XCTAssertNil(user?.accountDetails)
            
        }
        
        ws.parseUserInfosResponse(login: login, json: json)
    }
    
    func testAPIRandomDatas() {
        let json = [
            "theme": [
                "id": 1,
                "name": "Default",
                "font_name": "Helvetica Neue",
                "font_color": "#555555",
                "background_color": "#F5F5F5",
                "accent_color": "#999999",
                "button_color": "#468CC8",
                "show_author": true,
                "show_context": true,
                "show_tags": false,
                "show_source": false,
                "show_quotation_marks": false,
                "presentation_mode": false
            ]
            ] as [String : Any]
        
        let ws = UserService()
        ws.successHandler = { response in
            XCTFail()
        }
        
        ws.errorHandler = { error in
            
            XCTAssertNil(error)
            
        }
        
        ws.parseUserInfosResponse(login: "gose", json: json)
    }
}
