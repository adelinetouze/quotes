//
//  CreateSessionTests.swift
//  QuotesTests
//
//  Created by Adeline Touzé on 05/05/2018.
//  Copyright © 2018 AT. All rights reserved.
//

import XCTest

import RealmSwift
/** test
 setup : clear user session
 - check no user in DB
 - check no user_token
 - check no login
 
 teardown :
 - clear user in DB
 - clear user_token
 - clear login
 
 usecase : json with API error object
 > errorHandler with APIError object
 > successHandler not called
 > No user in DB, no user_token, no login
 
 usecase : json with User object
 > errorHandler not called
 > succesHandler with login
 > User in DB, user_token set, login set
 
 usecase : json with random data
 > errorHandler with nil object
 > succesHandler not called
 > No user in DB, no user_token, no login
 */

@testable import Quotes

class CreateSessionTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
        
        UserDefaults.standard.removeObject(forKey: "user_token")
        UserDefaults.standard.removeObject(forKey: "login")
        UserDefaults.standard.synchronize()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        super.tearDown()
    }
    
    func testAPIError() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let json = [
            "error_code" : 21,
            "message": "Invalid login or password."
            ] as [String : Any]
        
        let ws = UserService()
        ws.successHandler = { response in
            XCTFail()
        }
        
        ws.errorHandler = { error in
           
            XCTAssertNotNil(error)
            XCTAssertNil(UserDefaults.standard.string(forKey: "user_token"))
            XCTAssertNil(UserDefaults.standard.string(forKey: "login"))
            let realm = try! Realm()
            XCTAssertTrue(realm.objects(User.self).count == 0)
            
            XCTAssert(error!.isKind(of: APIError.self))
        }
        
        ws.parseCreateSessionResponse(json: json)
    }
    
    func testAPISuccess() {
    let json = [
        "User-Token" : "i4dwXAUNfSUEaTP90K5RWOWLap5cvc9mtjfpvA+c0uufP5GYnpYZUO4k5pTEpwsJHVHNhY5lbLn0cDRpiwsOKA==",
        "login" : "user_login",
        "email" : "user_email"
        ]
        
        let ws = UserService()
        
        ws.errorHandler = { _ in
            XCTFail()
            
        }
        
        ws.successHandler = { response in
            
            guard let login : String = response as? String else {
                XCTFail()
                return
            }
            
            XCTAssertEqual(login, "user_login")
            XCTAssertNotNil(UserDefaults.standard.string(forKey: "user_token"))
            XCTAssertNotNil(UserDefaults.standard.string(forKey: "login"))
            let realm = try! Realm()
            XCTAssertTrue(realm.objects(User.self).count == 1)
            
        }
        
        ws.parseCreateSessionResponse(json: json)
    }
    
    func testApiRandomResponse() {
        
        let json = [
            "theme": [
                "id": 1,
                "name": "Default",
                "font_name": "Helvetica Neue",
                "font_color": "#555555",
                "background_color": "#F5F5F5",
                "accent_color": "#999999",
                "button_color": "#468CC8",
                "show_author": true,
                "show_context": true,
                "show_tags": false,
                "show_source": false,
                "show_quotation_marks": false,
                "presentation_mode": false
            ]
            ] as [String : Any]
        
        let ws = UserService()
        ws.successHandler = { response in
            XCTFail()
        }
        
        ws.errorHandler = { error in
            
            XCTAssertNil(error)
            XCTAssertNil(UserDefaults.standard.string(forKey: "user_token"))
            XCTAssertNil(UserDefaults.standard.string(forKey: "login"))
            let realm = try! Realm()
            XCTAssertTrue(realm.objects(User.self).count == 0)
            
        }
        
        ws.parseCreateSessionResponse(json: json)
    }
}
