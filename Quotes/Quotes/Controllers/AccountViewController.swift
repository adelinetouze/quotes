//
//  AccountViewController.swift
//  Quotes
//
//  Created by Adeline Touzé on 08/05/2018.
//  Copyright © 2018 AT. All rights reserved.
//

import UIKit
import Alamofire

class AccountViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var favLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.loginLabel.font = Theme.regularFont(size: 18)
        self.loginLabel.textColor = Theme.qDarkGrey
        self.favLabel.font = Theme.regularFont(size: 18)
        self.favLabel.textColor = Theme.qDarkGrey
        
        self.imageView.layer.borderWidth = 1
        self.imageView.layer.masksToBounds = false
        self.imageView.layer.borderColor = Theme.qBlue.cgColor
        self.imageView.layer.cornerRadius = self.imageView.frame.height/2
        self.imageView.clipsToBounds = true
        
        self.updateUserInfos()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.displayUserInfos()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AccountViewController {
    // Manage user
    
    func updateUserInfos() {
        
        // user just logged, with no informations
        if let user = UserService.getCurrentUser(), user.accountDetails == nil {
            
            let ws = UserService()
            
            ws.errorHandler = {
                error in
                
                // TODO KVNProgress show error
            }
            
            ws.successHandler = {
                response in
                
                // TODO KVNProgress show success
                self.displayUserInfos()
            }
            
            ws.getCurrentUserInfos()
        }
            // FEATURE : cache request
        else {
            let ws = UserService()
            
            ws.errorHandler = {
                error in
                
                // TODO KVNProgress show error
            }
            
            ws.successHandler = {
                response in
                
                // TODO KVNProgress show success
                self.displayUserInfos()
            }
            
            ws.getCurrentUserInfos()
        }
        
    }
    
    func displayUserInfos() {
        
        if let user = UserService.getCurrentUser() {
            
            if let url = URL(string: user.picUrl) {
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async {
                    self.imageView.image = UIImage(data: data!)
                }
            }
            }
            
            self.loginLabel.text = user.login
            self.favLabel.text = String(format: NSLocalizedString("label.fav", comment: ""), user.publicFavoritesCount)
        }
    }
}
