//
//  ViewController.swift
//  Quotes
//
//  Created by Adeline Touzé on 04/05/2018.
//  Copyright © 2018 AT. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.setupDisplay()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupDisplay() {
        self.view.backgroundColor = Theme.backgroundColor
    
        self.loginField.font = Theme.regularFont(size: 18)
        self.loginField.textColor = Theme.qDarkGrey
        self.loginField.delegate = self
        self.loginField.placeholder = NSLocalizedString("Username or Email", comment: "")

        self.passwordField.font = Theme.regularFont(size: 18)
        self.passwordField.textColor = Theme.qDarkGrey
        self.passwordField.delegate = self
        self.passwordField.placeholder = NSLocalizedString("Password or Secret", comment: "")
        
        self.loginButton.backgroundColor = Theme.qBlue
        self.loginButton.setTitleColor(UIColor.white, for: .normal)
        self.loginButton.tintColor = UIColor.white
        self.loginButton.titleLabel?.font = Theme.boldFont(size: 20)
        self.loginButton.setTitle(NSLocalizedString("Sign In", comment: ""), for: .normal)
        self.loginButton.clipsToBounds = true
        self.loginButton.layer.cornerRadius = 5
    }

    @IBAction func loginAction(_ sender: Any) {
        
        guard let login = self.loginField.text, login.isEmpty == false, let password = self.passwordField.text , password.isEmpty == false else {
            return
        }
        
        let ws = UserService()
        
        ws.errorHandler = {
            error in
            
            // TODO KVNProgress show error
        }
        
        ws.successHandler = {
            response in
            
            // TODO KVNProgress show success
            
            self.dismiss(animated: true, completion: nil)
        }
        
        ws.createSession(login: login, password: password)
        
    }
}


extension ViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.loginField {
            self.passwordField.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.passwordField {
            // TODO login
        }
    }
}
