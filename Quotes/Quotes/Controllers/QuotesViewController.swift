//
//  QuotesViewController.swift
//  Quotes
//
//  Created by Adeline Touzé on 06/05/2018.
//  Copyright © 2018 AT. All rights reserved.
//

import UIKit
import RealmSwift

private let reuseIdentifier = "QuoteCell"
fileprivate let itemsPerRow : CGFloat = 2
fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 20.0, bottom: 10.0, right: 20.0)

class QuotesViewController: UICollectionViewController {
    
    var quotes : Results<Quote>?
    var quotesOrder : [Int] = [Int]()
    var notificationToken : NotificationToken? = nil
    
    deinit {
        notificationToken = nil
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        let nib = UINib(nibName: "QuoteCollectionViewCell", bundle: nil)
        self.collectionView!.register(nib, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
        
        let realm = try! Realm()
        self.quotes = realm.objects(Quote.self).sorted(byKeyPath: "body", ascending: true)
        self.quotesOrder = QuoteService.getQuotesOrder()
        
        notificationToken = self.quotes?.observe({ [weak self] (changes: RealmCollectionChange) in
            guard let collectionView = self?.collectionView else { return }
            switch changes {
            case .initial:
                // Results are now populated and can be accessed without blocking the UI
                self?.quotesOrder = QuoteService.getQuotesOrder()
                collectionView.reloadData()
                break
            case .update(_, _, _, _):
                // Query results have changed, so apply them to the UI
                self?.quotesOrder = QuoteService.getQuotesOrder()
                collectionView.reloadData()
                break
            case .error(let error):
                // An error occurred while opening the Realm file on the background worker thread
                fatalError("\(error)")
                break
            }
        })
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserService.getCurrentUser() == nil {
            self.performSegue(withIdentifier: "LoginSegue", sender: self)
        }
        else {
            self.loadQuotesData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.quotesOrder.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let quoteId = self.quotesOrder[indexPath.row]
        
        guard let quote : Quote = self.quotes?.filter("id = %d", quoteId).first else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
            return cell
        }
        
        if let video : VideoQuote = quote as? VideoQuote {
            // TODO create cell with viedo display
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
            
            // Configure the cell
            if let quoteCell : QuoteCollectionViewCell = cell as? QuoteCollectionViewCell {
                quoteCell.configure(quote: quote)
            }
            return cell
            
        }
    }
    

    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        print("Starting Index: \(sourceIndexPath.item)")
            print("Ending Index: \(destinationIndexPath.item)")
        
        let item = self.quotesOrder.remove(at: sourceIndexPath.item)
        self.quotesOrder.insert(item, at: destinationIndexPath.item)
        
        QuoteService.saveQuotesOrder(ids: self.quotesOrder)
    }
    
    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */


}

// Collection view cells display
extension QuotesViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem * 2)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        
        return sectionInsets
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
}

// Quotes
extension QuotesViewController {
    
    func loadQuotesData() {
        
        let ws = QuoteService()
        
        ws.errorHandler = {
            error in
            
            // TODO KVNProgress show error
        }
        
        ws.successHandler = {
            response in
            
            // TODO KVNProgress show success
        }
        
        ws.loadCurrentUserFavQuotes()
        
    }
    
}

// User management
extension QuotesViewController {
    
    
    @IBAction func accountAction(_ sender: Any) {
        
        if let _ = UserService.getCurrentUser() {
            
            
            // present with actionsheet
            let accountMenu = UIAlertController(title: "Your account", message: nil, preferredStyle: .actionSheet)
            
            // logout
            let destroyAction = UIAlertAction(title: "Logout", style: .destructive, handler: {
                (alert: UIAlertAction!) -> Void in
                self.destroySession()
            })
            let showAction = UIAlertAction(title: "Show account details", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                // TODO display account informations
                self.performSegue(withIdentifier: "AccountSegue", sender: self)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                (alert: UIAlertAction!) -> Void in
                // nothing to do
            })
            
            accountMenu.addAction(showAction)
            accountMenu.addAction(cancelAction)
            accountMenu.addAction(destroyAction)
            
            // display action sheet
            self.present(accountMenu, animated: true, completion: nil)

        }
    }
    
    func destroySession() {
        
        let ws = UserService()
        
        ws.errorHandler = {
            error in
            
            // TODO KVNProgress show error
        }
        
        ws.successHandler = {
            response in
            
            // TODO KVNProgress show success
            self.performSegue(withIdentifier: "LoginSegue", sender: self)
            
        }
        
        ws.destroySession()
        
    }
    
    
}
