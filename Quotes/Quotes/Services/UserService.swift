//
//  UserService.swift
//  Quotes
//
//  Created by Adeline Touzé on 05/05/2018.
//  Copyright © 2018 AT. All rights reserved.
//

import UIKit

import Alamofire
import ObjectMapper
import RealmSwift

class UserService {

    var successHandler : ((_ responseObject: AnyObject?) -> Void)?
    var errorHandler:((_ error: APIError?) -> Void)?
    
    class func getCurrentUser() -> User? {
        
        guard let login = UserDefaults.standard.string(forKey: "login") else {
            return nil
        }
        
        let realm = try! Realm()
        
        return realm.object(ofType: User.self, forPrimaryKey: login)
        
    }
    
    /**
     * Func call only once at login
     * User-Token has no duration time limit (time limit not specified in API documentation)
     */
    func createSession(login : String, password : String) {
        
        guard login.isEmpty == false && password.isEmpty == false else {
            self.errorHandler?(nil)
            return
        }
        
        Alamofire.request(FavQsRouter.createSession(login, password))
        .validate(statusCode: 200..<305)
        .responseJSON { response in
            switch response.result {
            case .success:
                
                guard let json : [String : Any] = response.result.value as? [String : Any], json.count > 0 else {
                    self.errorHandler?(nil)
                    return
                }
                
                self.parseCreateSessionResponse(json: json)
                
            case .failure(_):
                self.errorHandler?(nil)
            }
        }
        
    }
    
    func destroySession() {
        
        // check user have session
        guard UserDefaults.standard.string(forKey: "login") != nil else {
        self.errorHandler?(nil)
        return
    }
        
        Alamofire.request(FavQsRouter.destroySession)
            .validate(statusCode: 200..<305)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    if let json : [String : Any] = response.result.value as? [String : Any], let error : APIError = Mapper<APIError>().map(JSON: json) {
                        self.errorHandler?(error)
                    }
                    else {
                        UserDefaults.standard.removeObject(forKey: "login")
                        UserDefaults.standard.removeObject(forKey: "user_token")
                        UserDefaults.standard.removeObject(forKey: "quote_order")
                        UserDefaults.standard.synchronize()
                        self.successHandler?(true as AnyObject)
                    }
                case .failure(_):
                    self.errorHandler?(nil)
                }
        }
    }
    
    
    func getCurrentUserInfos() {
        
        guard let login = UserDefaults.standard.string(forKey: "login") else {
            self.errorHandler?(nil)
            return
        }
        
        self.getUserInfos(login: login)
    }
    
    func getUserInfos(login : String) {
        
        guard login.isEmpty == false else {
            errorHandler?(nil)
            return
        }
        Alamofire.request(FavQsRouter.user(login))
        .validate(statusCode: 200..<305)
            .responseJSON { response in
                switch response.result {
                case .success:
                    
                    guard let json : [String : Any] = response.result.value as? [String : Any], json.count > 0 else {
                        self.errorHandler?(nil)
                        return
                    }
                    
                    self.parseUserInfosResponse(login: login, json: json)
                    
                case .failure(_):
                    self.errorHandler?(nil)
                }
        }

    }
    
}

extension UserService {

    /**
     * Func created to make unit test easier
     * Call only by createSession func
     */
    func parseCreateSessionResponse(json : [String : Any]) {
        
        if let error : APIError = Mapper<APIError>().map(JSON: json) {
            self.errorHandler?(error)
        }
        else if let user : User = Mapper<User>().map(JSON: json) {
            
            let realm = try! Realm()
            try! realm.write {
                realm.add(user, update: true)
            }
            
            UserDefaults.standard.set(user.token, forKey: "user_token")
            UserDefaults.standard.set(user.login, forKey: "login")
            UserDefaults.standard.synchronize()
            
            successHandler?(user.login as AnyObject)

        }
        else {
            self.errorHandler?(nil)
            
        }
    }
    
    /**
     * Func created to make unit test easier
     ** Called only by getUserInfos func
    */
    func parseUserInfosResponse(login : String, json : [String : Any]) {
        
        let realm = try! Realm()
        
        if let error : APIError = Mapper<APIError>().map(JSON: json) {
            self.errorHandler?(error)
        }
        else if let old =  realm.object(ofType: User.self, forPrimaryKey: login) {
            
            try! realm.write {
                let user : User = Mapper<User>().map(JSON: json, toObject: old)
                
                realm.add(user, update: true)
            }
            
            self.successHandler?(login as AnyObject)
        }
        else if let user : User = Mapper<User>().map(JSON: json) {
            
            let realm = try! Realm()
            try! realm.write {
                realm.add(user, update: true)
            }
            
            self.successHandler?(login as AnyObject)
        }
        else {
            self.errorHandler?(nil)
            
        }
    }
}
