//
//  QuoteService.swift
//  Quotes
//
//  Created by Adeline Touzé on 08/05/2018.
//  Copyright © 2018 AT. All rights reserved.
//

import UIKit

import Alamofire
import AlamofireActivityLogger
import ObjectMapper
import RealmSwift

class QuoteService {

    var successHandler : ((_ responseObject: AnyObject?) -> Void)?
    var errorHandler:((_ error: APIError?) -> Void)?
    
    func loadUserFavQuotes(login : String, page : Int = 1) {
        
        guard login.isEmpty == false else {
            self.errorHandler?(nil)
            return
        }
        
        Alamofire.request(FavQsRouter.favQuotes(login, page))
            .validate(statusCode: 200..<305)
            .log()
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    if let json : [String : Any] = response.result.value as? [String : Any], let error : APIError = Mapper<APIError>().map(JSON: json) {
                        self.errorHandler?(error)
                    }
                    else if let json : [String : Any] = response.result.value as? [String : Any] {
                        
                        // TODO Map quotes
                        if let quotesArray : [[String : Any]] = json["quotes"] as? [[String : Any]] {
                            
                            let items = Mapper<Quote>().mapArray(JSONArray: quotesArray)
                            let arrayOfIDs = items.compactMap({ (quote) -> Int? in
                                return quote.id
                            })
                            
                            let realm = try! Realm()
                            let oldQuotes = realm.objects(Quote.self)
                            
                            let predicate = NSPredicate(format: "NOT (id IN %@)", arrayOfIDs)
                            let removed = oldQuotes.filter(predicate)
                            
                            try! realm.write {
                                realm.delete(removed)
                                realm.add(items, update: true)
                            }
                            
                            var itemsOrder = QuoteService.getQuotesOrder()
                            
                            if itemsOrder.count == 0 {
                                QuoteService.saveQuotesOrder(ids: arrayOfIDs)
                            }
                            else {
                                itemsOrder = itemsOrder.filter({ (quoteId) -> Bool in
                                    return arrayOfIDs.contains(quoteId)
                                })
                                
                                let oldIds = oldQuotes.compactMap({ (quote) -> Int? in
                                    return quote.id
                                })
                                
                                let added = arrayOfIDs.filter({ (newId) -> Bool in
                                    return !oldIds.contains(newId)
                                })
                                if added.count > 0 {
                                    
                                    itemsOrder += added
                                }
                                QuoteService.saveQuotesOrder(ids: itemsOrder)
                            }
                        }
                        
                        if let last : Bool = json["last_page"] as? Bool, last == false {
                            self.loadUserFavQuotes(login: login, page: page+1)
                        }
                        else {
                            self.successHandler?(true as AnyObject)
                        }
                        
                    }
                    else {
                        
                        self.errorHandler?(nil)
                    }
                case .failure(_):
                    self.errorHandler?(nil)
                }
        }
        
    }
    
    func loadCurrentUserFavQuotes() {
        
        guard let login = UserDefaults.standard.string(forKey: "login") else {
            self.errorHandler?(nil)
            return
        }
        
        self.loadUserFavQuotes(login: login)
    }
    
}

extension QuoteService {
    
    class func getQuotesOrder() -> [Int] {
        
        if let serialized = UserDefaults.standard.data(forKey: "quote_order"), let dictionary : [Int] = (try? JSONSerialization.jsonObject(with: serialized, options: .mutableLeaves)) as? [Int] {
            return dictionary
        }
        
        return [Int]()
    }
    
    class func saveQuotesOrder(ids : [Int]) {
        if let jsonData = try? JSONSerialization.data(withJSONObject: ids, options: []) {
            UserDefaults.standard.set(jsonData, forKey: "quote_order")
            UserDefaults.standard.synchronize()
        }
    }
}
