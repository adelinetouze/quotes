//
//  APIError.swift
//  Quotes
//
//  Created by Adeline Touzé on 05/05/2018.
//  Copyright © 2018 AT. All rights reserved.
//

import UIKit

import ObjectMapper

class APIError: NSObject, Mappable {

    var errorCode : Int?
    var message : String?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return APIError()
    }
    
    required init?(map: Map){
        // check if a required "error_code" property exists within the JSON.
        guard let _ = map.JSON["error_code"] else {
            return nil
        }
    }
    
    private override init(){}
    
    func mapping(map: Map)
    {
        
        errorCode <- map["error_code"]
        message <- map["message"]
    }

}
