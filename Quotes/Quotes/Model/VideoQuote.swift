//
//  VideoQuote.swift
//  Quotes
//
//  Created by Adeline Touzé on 09/05/2018.
//  Copyright © 2018 AT. All rights reserved.
//

import UIKit
import ObjectMapper

class VideoQuote: Quote {

    
    @objc dynamic var videourl : String = ""

    override class func objectForMapping(map: Map) -> BaseMappable? {
        
        
        return VideoQuote()
    }
    
    public required convenience init?(map: Map) {
        // check if a required "error_code" property exists within the JSON.
        guard let _ = map.JSON["id"] else {
            return nil
        }
        self.init()
        
        mapping(map: map)
    }
    
    public override func mapping( map: Map)
    {
        super.mapping(map: map)
        videourl <- map["video_url"]
    }
}
