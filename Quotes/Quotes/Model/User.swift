//
//  User.swift
//  Quotes
//
//  Created by Adeline Touzé on 04/05/2018.
//  Copyright © 2018 AT. All rights reserved.
//

import UIKit

import RealmSwift
import ObjectMapper

class User: Object, Mappable {
    
    @objc dynamic var token: String = ""
    @objc dynamic var login: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var picUrl: String = ""
    @objc dynamic var publicFavoritesCount: Int = 0
    @objc dynamic var followers: Int = 0
    @objc dynamic var following: Int = 0
    @objc dynamic var pro : Bool = false
    @objc dynamic var accountDetails : String? // TODO account details in object
    
    public override class func primaryKey() -> String? {
        return "login"
    }
    
    public required convenience init?(map: Map) {
        // check if a required "error_code" property exists within the JSON.
        guard let _ = map.JSON["login"] else {
            return nil
        }
        
        self.init()
        mapping(map: map)
    }
    
    public func mapping( map: Map)
    {
        if login.isEmpty {
            login <- map["login"]
        }
        token <- map["User-Token"]
        email <- map["email"]
        picUrl <- map["pic_url"]
        publicFavoritesCount <- map["public_favorites_count"]
        followers <- map["followers"]
        following <- map["following"]
        pro <- map["pro"]
        //accountDetails <- map["account_details"] // TODO map account details object
    }
    
}
