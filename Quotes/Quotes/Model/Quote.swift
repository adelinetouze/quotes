//
//	Quote.swift
//
//	Create by Mukesh Yadav on 8/5/2018

import Foundation 
import ObjectMapper
import RealmSwift


class Quote : Object, StaticMappable {

	@objc dynamic var author : String = ""
	@objc dynamic var authorPermalink : String = ""
	@objc dynamic var body : String = ""
	@objc dynamic var dialogue : Bool = false
	@objc dynamic var downvotesCount : Int = 0
	@objc dynamic var favoritesCount : Int = 0
	@objc dynamic var id : Int = 0
	@objc dynamic var privateField : Bool = false
	@objc dynamic var tags : String = ""
	@objc dynamic var upvotesCount : Int = 0
	@objc dynamic var url : String = ""
    //@objc dynamic var userDetails : UserDetail?
    // TODO user detail

    class func objectForMapping(map: Map) -> BaseMappable? {
    
        guard let _ = map.JSON["id"] else {
            return nil
        }
        
        if map.JSON["video_url"] != nil {
            return VideoQuote()
        }
        
        return Quote()
    }
    
    public override class func primaryKey() -> String? {
        return "id"
    }
    
    public required convenience init?(map: Map) {
        // check if a required "error_code" property exists within the JSON.
        guard let _ = map.JSON["id"] else {
            return nil
        }
        self.init()
        
        mapping(map: map)
    }
    
    
	func mapping(map: Map)
	{
		author <- map["author"]
		authorPermalink <- map["author_permalink"]
		body <- map["body"]
		dialogue <- map["dialogue"]
		downvotesCount <- map["downvotes_count"]
		favoritesCount <- map["favorites_count"]
		id <- map["id"]
		privateField <- map["private"]
        if let items : [String] = map["tags"].currentValue as? [String] {
            tags = items.joined(separator: ", ")
        }
		upvotesCount <- map["upvotes_count"]
		url <- map["url"]
		//userDetails <- map["user_details"]
		
	}

    var displayString : String {
        get {
            var item = "Author : " + author + "\n"
            item = item + "Tags : " + tags + "\n"
            item = item +  (dialogue ? "is from dialogue" : "is not form dialogue") + "\n"
            item = item + "Votes : (+)\(upvotesCount) | (-)\(downvotesCount)" + "\n"
            item = item + "Favorites : \(favoritesCount)" + "\n"
            item = item + (privateField ? "Private quote" : "Public quote") + "\n"
            
            return item
        }
    }

}
