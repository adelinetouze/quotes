//
//  QuoteCollectionViewCell.swift
//  Quotes
//
//  Created by Adeline Touzé on 08/05/2018.
//  Copyright © 2018 AT. All rights reserved.
//

import UIKit

class QuoteCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.titleLabel.font = Theme.boldFont(size: 18)
        self.titleLabel.textColor = Theme.qDarkGrey
        self.descriptionLabel.font = Theme.regularFont(size: 18)
        self.descriptionLabel.textColor = Theme.qDarkGrey
    }
    
    func configure(quote : Quote) {
        
        self.titleLabel.text = quote.body
        self.descriptionLabel.text = quote.displayString
    }

}
