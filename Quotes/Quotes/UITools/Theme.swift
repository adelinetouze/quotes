//
//  Theme.swift
//  LeaderPriceBorne
//
//  Created by Adeline Touzé on 30/01/2018.
//  Copyright © 2018 Carving Labs. All rights reserved.
//

import Foundation
import UIKit

class Theme {
    
    public class func regularFont(size : CGFloat = 18) -> UIFont? {
        return UIFont.init(name: "Cochin", size: size)
    }
    
    public class func boldFont(size : CGFloat = 18) -> UIFont? {
        return UIFont.init(name: "Cochin-Bold", size: size)
    }
    
    public class var backgroundColor : UIColor {
        return UIColor.white
    }
    
    public class var titleColor : UIColor {
        return UIColor.red
    }
    
    public class var textColor : UIColor {
        return UIColor.blue
    }
    
    public class var fieldColor : UIColor {
        return UIColor.darkGray
    }
    
    public class var qBlue : UIColor {
        return UIColor.init(red: 36.0/255.0, green: 67.0/255.0, blue: 148.0/255.0, alpha: 1.0)
    }
    
    public class var qRed : UIColor {
        return UIColor.init(red: 230.0/255.0, green: 31.0/255.0, blue: 33.0/255.0, alpha: 1.0)
    }
    
    public class var qDarkGrey : UIColor {
        return UIColor.init(red: 69.0/255.0, green: 69.0/255.0, blue: 69.0/255.0, alpha: 1.0)
    }
}
