

import Foundation

import Alamofire

enum FavQsRouter: URLRequestConvertible {
    
    static let BaseURI = "https://favqs.com/api"
    
    case createSession(String, String)
    case destroySession
    case user(String)
    
    case dailyQuote
    case favQuotes(String, Int)
    
    var method: Alamofire.HTTPMethod {
        switch self {
        case .createSession(_, _):
            return .post
        case .destroySession:
            return .delete
        case .user(_), .dailyQuote, .favQuotes :
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .createSession(_, _), .destroySession :
            return "/session"
        case .user(let login) :
            return "/users/\(login)"
        case .dailyQuote :
            return "/qotd"
        case .favQuotes(_, _) :
            return "/quotes"
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        
        let URL : Foundation.URL = Foundation.URL(string: FavQsRouter.BaseURI)!
        
        var mutableURLRequest = URLRequest(url: URL.appendingPathComponent(path))
        mutableURLRequest.httpMethod = method.rawValue
        
        // si need change timeout
        //mutableURLRequest.timeoutInterval =
        
        mutableURLRequest.setFavQsHeaders()
        
        switch self {
        case .createSession(let login, let password):
            return try Alamofire.JSONEncoding.default.encode(mutableURLRequest as URLRequestConvertible, with: ["user" : ["login" : login, "password" : password]])
        case .favQuotes(let login, let page) :
            return try Alamofire.URLEncoding.default.encode(mutableURLRequest as URLRequestConvertible, with: ["filter" : login, "type" : "user", "page" : page])
        default:
            return mutableURLRequest as URLRequest
        }
    }
}
