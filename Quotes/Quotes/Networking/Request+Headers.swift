
import Foundation

extension URLRequest {
    
    static let FavQsAppToken = "e09fb0fba324951c821394c2dd75a5d8"
    
    public mutating func setFavQsHeaders() {

        if let userid = UserDefaults.standard.string(forKey: "user_token") { 
            self.setValue( userid, forHTTPHeaderField: "User-Token")
        }

        self.setValue("application/vnd.favqs.v2+json", forHTTPHeaderField: "Accept")
        self.setValue("application/json", forHTTPHeaderField: "Content-Type")
        self.setValue("Token token=\"\(URLRequest.FavQsAppToken)\"", forHTTPHeaderField: "Authorization")
        
        // FEATURE Caching request
        
    }

}
